const express = require("express");
const morgan = require('morgan');
const app = express();
const bodyParser = require("body-parser");
const multer = require('multer');
const cors = require('cors');
const mongoose = require("mongoose");
const mysql = require('mysql');
const expressValidator = require('express-validator');
const fs = require('fs');
const path = require('path');
const passport = require("passport");
const session = require("express-session");
const compression = require('compression');
const config = require('./config/app.config');
const dbConfig = require('./config/dbconfig');
const routes = require('./routes/routes');

//Body-Parser config

app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));

//DB Configuration 

mongoose.connect(dbConfig.url);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
 // we're connected!
 console.log("Database Connection Established");
})

//Configure express session

app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));

//Initialise Passport and Use Session

app.use(passport.initialize());
app.use(passport.session());

//Using Express Validator

app.use(expressValidator());

//Using CORS

app.use(cors());

//Logger function
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {
    flags: 'a'
   })
   app.use(morgan('dev', {
    stream: accessLogStream
   }))

//Defalut Router

app.get('/', function(req, res){
    res.send("Welcome to LALA API Service");
})


//Router Initialization

app.use('/api/v1',routes);

//Server Initialization

app.listen(config.port, config.host, () => {
    console.log('Server started on ' + new Date());
    console.log(`server is running at http://${config.host}:${config.port}`);
});