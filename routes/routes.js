const express = require("express");
const router = express.Router();
const user = require('../controllers/users');


//Users Routing

router.post('/user', user.create);


module.exports = router;