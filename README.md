## *Node.js Bolier Plate*
This repo contains the basic folder structure and implementation of Express framework upon Node.js. The following functionalities are implemented using the corresponding modules.

| Functionalities | Modules / Framework |
|---|---|
| Server | express |
| Routing               | express-router |
| Body Parser           | body-parser |
| Cors Domain Access    | cors |
| Logging               | morgan |
| Mailing               | nodemailer |
| Authentication        | passport |
| Validation            | express-validator |
| Compression           | compression |
| File System           | fs |
| Path                  | path |
| External API call     | request |
| MongoDB               | mongoose |
| MySql                 | mysql |
| Session Mgmt.         | express-session |
| Environment variables | dotenv |
| File Upload           | multer |
| Promise               | bluebird |
| Server Management     | Production - pm2, Development - nodemon |

# Variables and import function .

 - Variables, properties and function names should use `lowerCamelCase`.They should also be descriptive. Single character variables and uncommon abbreviations should generally be avoided.
 
```javascript
   var adminUser = 'Anuj Tiwari';
```
   
- The import function should use const keyword for requiring the modules into the projects.
    
```javascript
    const express = require('express');
```

## Note 
- The Node project should run on port 3000, 3001, 3002.
- While starting the project using pm2 mention the name for easy identifications.
- The server start date and time need to be consoled on the terminal and log files.
- The log files need to be maintained in the same project folder.
- The code needs to beautify before pushing to the GitHub from local machine using ides.
