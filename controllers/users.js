const User = require("../models/users");

exports.create = function (req, res) {

    var newUser = {
        username: req.body.userName,
        password: req.body.password

    }

    User.create(newUser, function (err, data) {
        if (err) {
            res.status(200).json({
                'statuscode': 'ERR',
                'message': 'Request Failed',
                'data': err
            })
        } else {

            res.status(200).json({
                'statuscode': 'SUC',
                'message': 'Request Completed Successfully',
                'data': data
            })
        }
    });
}